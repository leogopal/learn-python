# creates a cars variable and gives it a value of 100
cars = 100
# creates a space_in_a_car variable and gives it a value of 4.0
space_in_a_car = 4.0
# creates a drivers variable and gives it a value of 30
drivers = 30
# creates a passengers variable and gives it a value of 90
passengers = 90
# creates a cars_not_driven variable and gives it a value that is the difference between cars and drivers
cars_not_driven = cars - drivers
# creates a cars_driven variable and matches it with drivers
cars_driven = drivers
# creates a carpool_capacity variable and gives it the value of cars_driven multiplied by space_in_a_car
carpool_capacity = cars_driven * space_in_a_car
#creates an average_passengers_per_car and gives it the value of passengers divided by cars_driven
average_passengers_per_car = passengers / cars_driven

# There are100 cars available.
print "There are", cars, "cars available."
# There are only 30 drivers available.
print "There are only", drivers, "drivers available."
# There will be 70 empty cars today.
print "There will be", cars_not_driven, "empty cars today."
# We can transport 120 people today
print "We can transport", carpool_capacity, "people today."
# We have 90 passengers to carpool today
print "We have", passengers, "to carpool today."
# We need to put about 3 in each car
print "We need to put about", average_passengers_per_car, "in each car."